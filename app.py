# -*- coding: utf-8 -*-
"""
Team 21
Jack Stinson - 695666
Michael Milton - 637343
Koray Edib - 805529
Scott Lee - 793264
Jordan Lo Presti -640011

app.py - this program runs a basic front-end UI using dash to visualise the data
stored in the database
"""


import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from cloudant import couchdb
import argparse
import db_api

MATCHED = True
UNMATCHED = False

def get_args():
    parser = argparse.ArgumentParser('Runs the AFL Scraper frontend')
    parser.add_argument(
        '--db-host',
        help='The URL at which to connect to the database',
        required=True
    )
    return parser.parse_args()


args = get_args()
with couchdb('admin', 'Vai9vah6', url=args.db_host) as couch:
    print('connected...')
    db = couch['comp90024']
    app = dash.Dash()

    teams = db_api.win_loss_avg(db, MATCHED)
    unmatched_teams = db_api.win_loss_avg(db, UNMATCHED)
    seasons = db_api.win_loss_avg_season(db, MATCHED)
    unmatched_seasons = db_api.win_loss_avg_season(db, UNMATCHED)

    # print(teams.get_team_names())
    # for team in teams:
    #     print (team.winner.sentiment_avg)
    #     print (team.loser.sentiment_avg)

    # teams = db_api.win_loss_avg_season(db)
    # for team in teams:
    #     for season in team.seasons:
    #         print (season.winner.sentiment_avg)

    df = {'Adelaide': [350, 89], 'Brisbane Lions': [276, 150]}
    app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})
    app.layout = html.Div(children=[

        html.Div([
            html.H1(
                'All Seasons Average'
            )
        ], style={'margin-left': '20px'}),

        html.Div([
            dcc.Dropdown(
                id='all-xaxis-column',
                options=[{'label': i, 'value': i} for i in teams.get_team_names()],
                value='Adelaide'
            )
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        dcc.Graph(id='win-loss-avg-graphic'),

        html.Div([
            html.H1(
                'Average by Season'
            )
        ], style={'margin-left': '20px'}),

        html.Div([
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in teams.get_team_names()],
                value='Adelaide'
            )
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        html.Div([
            dcc.Dropdown(id='season-year-dropdown')
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        dcc.Graph(id='season-win-loss-avg-graphic'),

        html.Div([
            html.H1(
                'All Tweets Sentiments'
            )
        ], style={'margin-left': '20px'}),

        html.Div([
            dcc.Dropdown(
                id='general-team',
                options=[{'label': i, 'value': i} for i in unmatched_teams.get_team_names()],
                value='Adelaide'
            )
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        html.Div([
            dcc.Dropdown(id='general-season-year-dropdown')
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        dcc.Graph(id='general-by-season-graphic'),

        html.Div([
            html.H1(
                'Games Won Positive Sentiments'
            )
        ], style={'margin-left': '20px'}),

        html.Div([
            dcc.Dropdown(
                id='games-won-team',
                options=[{'label': i, 'value': i} for i in teams.get_team_names()],
                value='Adelaide'
            )
        ], style={'width': '25%', 'display': 'inline-block', 'margin': '0px 10px'}),

        dcc.Graph(id='games-won-graphic')

    ])

    @app.callback(
        dash.dependencies.Output('season-year-dropdown', 'options'),
        [dash.dependencies.Input('xaxis-column', 'value')])
    def set_year_options(value):
        years = [{'label': i, 'value': i} for i in seasons.get_team(value).get_season_years()]
        return years[::-1]


    @app.callback(
        dash.dependencies.Output('season-year-dropdown', 'value'),
        [dash.dependencies.Input('season-year-dropdown', 'options')])
    def set_year_value(available_options):
        return available_options[0]['value']

    @app.callback(
        dash.dependencies.Output('general-season-year-dropdown', 'options'),
        [dash.dependencies.Input('general-team', 'value')])
    def set_year_options(value):
        years = [{'label': i, 'value': i} for i in unmatched_seasons.get_team(value).get_season_years()]
        return years[::-1]


    @app.callback(
        dash.dependencies.Output('general-season-year-dropdown', 'value'),
        [dash.dependencies.Input('general-season-year-dropdown', 'options')])
    def set_year_value(available_options):
        return available_options[0]['value']


    @app.callback(
        dash.dependencies.Output('win-loss-avg-graphic', 'figure'),
        [dash.dependencies.Input('all-xaxis-column', 'value')])
    def update_graph(xaxis_column_name):
        emoji_data = []
        text_data = []
        win_data = []
        emoji_data.append(teams.get_team(xaxis_column_name).winner.sentiment_avg)
        emoji_data.append(teams.get_team(xaxis_column_name).winner.emoji_avg)
        emoji_data.append(teams.get_team(xaxis_column_name).win_percentage)
        text_data.append(teams.get_team(xaxis_column_name).loser.sentiment_avg)
        text_data.append(teams.get_team(xaxis_column_name).loser.emoji_avg)
        text_data.append(teams.get_team(xaxis_column_name).loss_percentage)

        winner = go.Bar(
            x=['Text Sentiment', 'Emoji Sentiment', 'Win/Loss'],
            y=emoji_data,
            name="Winning Matches",
            marker=dict(
                color='rgb(85, 224, 166)'
            )
        )

        loser = go.Bar(
            x=['Text Sentiment', 'Emoji Sentiment', 'Win/Loss'],
            y=text_data,
            name="Losing Matches",
            marker=dict(
                color='rgb(255, 102, 102)'
            )
        )

        result = go.Bar(
            )

        return {
            'data': [winner, loser],

            'layout': go.Layout(
                barmode='group',
                xaxis={
                    'title': xaxis_column_name
                }
            )
        }


    @app.callback(
        dash.dependencies.Output('season-win-loss-avg-graphic', 'figure'),
        [dash.dependencies.Input('xaxis-column', 'value'),
         dash.dependencies.Input('season-year-dropdown', 'value')])
    def update_graph(xaxis_column_name, season_year):
        emoji_data = []
        text_data = []

        emoji_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).winner.sentiment_avg)
        text_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).loser.sentiment_avg)
        emoji_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).winner.emoji_avg)
        text_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).loser.emoji_avg)
        emoji_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).win_percentage)
        text_data.append(seasons.get_team(xaxis_column_name).get_season(season_year).loss_percentage)

        winner = go.Bar(
            x=['Text Sentiment', 'Emoji Sentiment', 'Win/Loss'],
            y=emoji_data,
            name="Winning Matches",
            marker=dict(
                color='rgb(85, 224, 166)'
            )
        )

        loser = go.Bar(
            x=['Text Sentiment', 'Emoji Sentiment', 'Win/Loss'],
            y=text_data,
            name="Losing Matches",
            marker=dict(
                color='rgb(255, 102, 102)'
            )
        )

        return {
            'data': [winner, loser],

            'layout': go.Layout(
                barmode='group',
                xaxis={
                    'title': xaxis_column_name
                }
            )
        }

    @app.callback(
        dash.dependencies.Output('general-by-season-graphic', 'figure'),
        [dash.dependencies.Input('general-team', 'value'),
         dash.dependencies.Input('general-season-year-dropdown', 'value')])
    def update_graph(team_name, season_year):
        data = []

        data.append(unmatched_seasons.get_team(team_name).get_season(season_year).general.sentiment_pos)
        data.append(unmatched_seasons.get_team(team_name).get_season(season_year).general.sentiment_neg)

        winner = go.Bar(
            x=['Positive Sentiments', 'Negative Sentiments'],
            y=data,
            marker=dict(
                color='rgb(85, 224, 166)'
            )
        )

        return {
            'data': [winner],

            'layout': go.Layout(
                xaxis={
                    'title': team_name
                }
            )
        }

    @app.callback(
        dash.dependencies.Output('games-won-graphic', 'figure'),
        [dash.dependencies.Input('games-won-team', 'value')])
    def update_graph(team):
        years = seasons.get_team(team).get_season_years()
        win_percentages = []
        pos_size = []
        neg_size = []
        for year in years:
            if (seasons.get_team(team).get_season(year).loser == None or seasons.get_team(team).get_season(year).winner == None):
                continue
            win_percentages.append(seasons.get_team(team).get_season(year).win_percentage * 100)
            pos_count = seasons.get_team(team).get_season(year).winner.sentiment_pos
            neg_count = seasons.get_team(team).get_season(year).winner.sentiment_neg
            pos_percentage = pos_count / (pos_count + neg_count)
            neg_percentage = neg_count / (pos_count + neg_count)
            pos_size.append(pos_percentage * 100)
            neg_size.append(neg_percentage * 100)

        print(years)
        print(win_percentages)

        trace0 = go.Scatter(
            x=years,
            y=win_percentages,
            mode='markers',
            name='Positive Sentiments',
            marker=dict(
                    size=pos_size,
                    color='rgb(85, 224, 166)'
                )
        )

        trace1 = go.Scatter(
            x=years,
            y=win_percentages,
            mode='markers',
            name='Negative Sentiments',
            marker=dict(
                    size=neg_size,
                    color='rgb(255, 102, 102)'
                )
        )
        return {
            'data': [trace0, trace1],

            'layout': go.Layout(
                    xaxis={
                        'title': 'Season'
                    },
                    yaxis={
                        'title': 'Percentage of Games Won'
                    }
                )
            }

if __name__ == '__main__':
    app.run_server(host='0.0.0.0')
