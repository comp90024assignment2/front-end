"""
Team 21
Jack Stinson - 695666
Michael Milton - 637343
Koray Edib - 805529
Scott Lee - 793264
Jordan Lo Presti -640011

db_api.py - this provides the front-end program an easier to use, object style
way of accessing the relevant views in the database
"""

class AllTeams:
    """
    Class that keeps all teams
    """
    def __init__(self):
        self._teams = []

    def add_team(self, team):
        self._teams.append(team)

    def get_team_names(self):
        return [team.name for team in self._teams]

    def get_team(self, team_name):
        for team in self._teams:
            if team.name == team_name:
                return team

    def __iter__(self):
        for team in self._teams:
            yield team

class ScoresObject:
    """
    Inherited by other objects, contains values and functions
    for score related purposes
    """
    def __init__(self):
        self.winner = None
        self.loser = None
        self.general = None
        self.win_percentage = None
        self.loss_percentage = None

    def add_winning_scores(self, score):
        self.winner = score

    def add_losing_scores(self, score):
        self.loser = score

    def add_general_scores(self, score):
        self.general = score

    def add_percentages(self, win, loss):
        self.win_percentage = win;
        self.loss_percentage = loss;


class Team(ScoresObject):
    """
    Class to store data about a team
    """
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.seasons = []

    def get_season(self, year):
        for season in self.seasons:
            if season.year == year:
                return season

    def get_season_years(self):
        return [season.year for season in self.seasons]

    def add_season(self, year):
        self.seasons.append(Season(year))

    def __iter__(self):
        for season in self.seasons:
            yield season

    def __str__(self):
        return "Team: " + self.name + \
        " Scores: " + self.winner + " " + self.loser

class Season(ScoresObject):
    def __init__(self, year):
        super().__init__()
        self.year = year

class Scores:
    def __init__(self, values):
        self.emoji_count = values['emoji_count']
        self.emoji_avg = values['emoji_avg']
        self.sentiment_pos = values['count_pos']
        self.sentiment_neg = values['count_neg']
        self.sentiment_avg = values['sentiment_avg']

    def __str__(self):
        return "(Emoji Average: " + self.emoji_avg + \
         ", Sentiment Average: " + self.sentiment_avg + ")"


def win_loss_avg(db, isMatched):
    """
    Gives back data for each team's tweet data over ALL seasons
    returns 'AllTeams' obj that can be iterated over to yield team data
    if isMatched is true, returns only tweets that belong to matches
    """
    teams = AllTeams()

    view_name = 'allSentiment'
    group_level = 1

    if isMatched:
        view_name = 'allMatchSentiment'
        group_level = 2

    for row in db.get_view_result(
        '_design/tweets',
        view_name=view_name,
        group_level=group_level,
    ):
        team_name = row['key'][0]
        if team_name not in teams.get_team_names():
            teams.add_team(Team(team_name))

        team = teams.get_team(team_name)
        scores = Scores(row['value'])

        if isMatched:
            if row['key'][1] == 'winner':
                team.add_winning_scores(scores)
            else:
                team.add_losing_scores(scores)
        else:
            team.add_general_scores(scores)

    for row in db.get_view_result(
        '_design/matches',
        view_name='winLossPercentage',
        group_level=1
    ):

        team_name = row['key'][0]
        if team_name not in teams.get_team_names():
            continue
        else:
            team = teams.get_team(team_name)

        win_percentage = row['value']['win_percentage']
        loss_percentage = row['value']['loss_percentage']
        team.add_percentages(win_percentage, loss_percentage)

    return teams



def win_loss_avg_season(db, isMatched):
    """
    Gives back data for each team's tweet data PER season
    returns 'AllTeams' obj that can be iterated over to yield team data
    if isMatched is true, returns only tweets that belong to matches
    """

    teams = AllTeams()

    view_name = 'allSentiment'
    group_level = 2
    year_index = 1

    if isMatched:
        view_name = 'allMatchSentiment'
        group_level = 3
        year_index = 2

    for row in db.get_view_result(
        '_design/tweets',
        view_name=view_name,
        group_level=group_level,
    ):
        team_name = row['key'][0]
        if team_name not in teams.get_team_names():
            teams.add_team(Team(team_name))
        team = teams.get_team(team_name)

        year = row['key'][year_index]
        if year not in team.get_season_years():
            team.add_season(year)
        season = team.get_season(year)

        scores = Scores(row['value'])

        if isMatched:
            if row['key'][1] == 'winner':
                season.add_winning_scores(scores)
            else:
                season.add_losing_scores(scores)
        else:
            season.add_general_scores(scores)

    for row in db.get_view_result(
        '_design/matches',
        view_name='winLossPercentage',
        group_level=2
    ):

        team_name = row['key'][0]
        if team_name not in teams.get_team_names():
            continue
        else:
            team = teams.get_team(team_name)

        year = row['key'][1]
        if year not in team.get_season_years():
            continue
        else:
            season = team.get_season(year)

        win_percentage = row['value']['win_percentage']
        loss_percentage = row['value']['loss_percentage']
        season.add_percentages(win_percentage, loss_percentage)

    return teams
